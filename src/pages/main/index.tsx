import { useState, useEffect } from 'react';
import { Formik, Field } from 'formik';
import { MainInput } from 'components/ui/input';
import AskService from 'services/ask.service';
import { IQuestion } from 'services/ask.service';
import { Icon } from 'components/ui/icon';
import { IconButton } from 'components/ui/iconButton';
import styles from 'pages/main/index.module.sass';
import { useOutsideClick } from 'utils/outsideClick';
import { Prompt } from 'components/prompt';
import { AnswerCard } from 'components/answerCard';
import { IAnswer } from 'components/answerCard/index.interfaces';
import { Loader } from 'components/ui/loader'

export const MainPage = () => {
    const [dropdownShown, setDropdownShown] = useState(false);
    const [loaderShown, setLoaderShown] = useState(false);
    const [questionPrompts, setQuestionPrompts] = useState<string[]>([]);
    const [answer, setAnswer] = useState<IAnswer | null>(null);
    
    const openDropdown = () => {
        setDropdownShown(true);
    }
    const closeDropdown = () => {
        setDropdownShown(false);
    }
    useEffect(() => {
        const handleDropdown = () => {
            if (questionPrompts?.length > 0) {
                openDropdown();
            } else {
                closeDropdown();
            }
        };
        handleDropdown();
    }, [questionPrompts])

    const ref = useOutsideClick(closeDropdown);

    const sendQuestion = (question: IQuestion) => {
        console.log(question);
        setLoaderShown(true);
        setAnswer(null);
        AskService.ask(question)
            .then(data => {
                setAnswer(data);
                setLoaderShown(false);
            })
            .catch (err => {
                setAnswer({    
                    text: 'Недопустимый тип вопроса',
                    emphasis_start: 0,
                    emphasis_end: 0,
                });
                setLoaderShown(false);
        });
    }

    const updatePrompts = (e: React.ChangeEvent<any>, values: IQuestion) => {
        const inputEvent = e.nativeEvent as InputEvent;
        const keyValue = inputEvent.data;
        let questionPart = '';
        if (keyValue && inputEvent.inputType === 'insertText') {
            questionPart = values.question.concat(keyValue);
        } else if (inputEvent.inputType === 'deleteContentBackward') {
            questionPart = values.question.slice(0, -1);
        }
        AskService.getPrompts(questionPart)
            .then(prompts => {
                setQuestionPrompts(prompts);
            })
    }

    return (
        <div className={styles.screen}>
            <div className={styles.content}>
                <div>
                    <Formik
                        initialValues={{question: ""}}
                        onSubmit={(values) => {
                            sendQuestion(values);
                        }}
                    >
                        {props => {
                            const {
                                values,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                dirty, 
                                setFieldValue,  
                            } = props;
                            return (
                                <>
                                    <form onSubmit={handleSubmit} className={styles.form} ref={ref}>
                                        <Icon name="search" />
                                        <Field
                                            id='question'
                                            name='question'
                                            component={MainInput}
                                            values={values.question}
                                            onChange={(e: React.ChangeEvent<any>) => {
                                                handleChange(e);
                                                updatePrompts(e, values);
                                            }}
                                            onBlur={handleBlur}
                                            type='text'
                                            placeholder='Введите вопрос по линейной алгебре...'
                                        />
                                        <IconButton type="submit" iconName="send" disabled={!dirty}/>
                                    </form>
                                    <div className={styles.dropdown}>
                                    {
                                        dropdownShown
                                        && 
                                        <ul className={styles.dropdownItems}>
                                            {
                                                questionPrompts?.map((q, idx) => {
                                                    if (idx < 10)
                                                        return (
                                                            <li key={idx}>
                                                                <Prompt 
                                                                    onClick={
                                                                        () => {
                                                                            setFieldValue('question', q);
                                                                            sendQuestion({question: q});
                                                                        }
                                                                    } 
                                                                    text={q}
                                                                />
                                                            </li>
                                                        )
                                                    return null;
                                                })
                                            }
                                        </ul>
                                    }              
                                    </div>
                                </>
                            )
                        }}
                    </Formik>
                </div>
                {
                    loaderShown && <Loader />
                }
                {
                    answer && <AnswerCard answer={answer} />
                }
            </div>
        </div>
    )
}

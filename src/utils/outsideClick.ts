import { useRef, useEffect } from "react"
import assertIsNode from "./nodeAssertion";

export const useOutsideClick = (callback: any) => {
    const ref = useRef<HTMLFormElement>(null);

    useEffect(() => {
        const handleClick = ({target}: MouseEvent): void => {
            assertIsNode(target);
            if (
                ref.current 
                && !ref.current.contains(target) 
            ) {
                callback();
            } 
        }

        document.addEventListener('click', handleClick);

        return () => {
            document.removeEventListener('click', handleClick);
        };
    }, [ref]);

    return ref;
}

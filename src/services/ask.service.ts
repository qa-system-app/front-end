import api from "services/api.service";

export interface IQuestion {
    question: string,
};

const ask = ({question}: IQuestion) => {
    return api
        .get('/get_answer', {params: {question: question}})
        .then(reaponse => {
            console.log(reaponse.data);
            return reaponse.data;
        });
};

const getPrompts = (question_part: string) => {
    return api
        .get('/get_prompts', {params: {question_part: question_part}})
        .then(response => {
            return response.data;
        })
        .catch(error => console.log(error));
}

const AskService = {
    ask,
    getPrompts,
}

export default AskService;

import {IButton} from "components/ui/button/index.interfaces";
import styles from 'components/ui/button/index.module.sass';
import {FC} from "react";

export const Button: FC<IButton> = ({
    children,
    onClick,
    type='button',
    mode='primary',
    disabled,
    ...props
}) => {
    return (
        <button
            onClick={onClick}
            className={styles.button}
            type={type}
            disabled={disabled}
            {...props}
        >
            {children}
        </button>
    )
}

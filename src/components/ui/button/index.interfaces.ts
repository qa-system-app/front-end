import React from "react";

export interface IButton {
    children: React.ReactNode;
    onClick?: () => void,
    type?: 'submit' | 'reset' | 'button',
    mode?: 'primary' | 'secondary',
    disabled?: boolean,
}

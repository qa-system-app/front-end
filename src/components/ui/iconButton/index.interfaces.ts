export interface IIconButton {
    type?: 'button' | 'submit',
    onClick?: () => void,
    kind?: 'passwordVisibility',
    iconName: string,
    disabled?: boolean,
}

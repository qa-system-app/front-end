import {FC} from "react";
import styles from 'components/ui/iconButton/index.module.sass';
import { IIconButton } from "components/ui/iconButton/index.interfaces";
import {Icon} from "components/ui/icon";

export const IconButton: FC<IIconButton> = ({
    type='button',
    onClick,
    kind,
    iconName,
    disabled,
    ...props
}) => {
    return (
        <button
            onClick={onClick}
            className={styles.button}
            type={type}
            disabled={disabled}
            {...props}
        >
            <Icon name={iconName} />
        </button>
    )
}

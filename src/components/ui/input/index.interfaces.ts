export interface IInput {
    type?: 'text' | 'date' | 'checkbox' | 'password' | 'tel' | 'email' | 'number' | 'image',
    label?: string,
    id?: string,
}

import {FC} from "react";
import {FieldProps} from "formik";
import styles from 'components/ui/input/index.module.sass';
import {IInput} from "components/ui/input/index.interfaces";

export const MainInput: FC<IInput & FieldProps> = ({
        field,
        form,
        type='text',
        ...props
    }) => {

    return (
                <input
                    type={type}
                    {...field}
                    {...props}
                    className={styles.input}
                />
    )
}

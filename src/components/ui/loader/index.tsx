import styles from 'components/ui/loader/index.module.sass';

export const Loader = () => {
    return (
        <div className={styles.dots}>
            <div></div>
            <div></div>
            <div></div>
        </div>
    );
}

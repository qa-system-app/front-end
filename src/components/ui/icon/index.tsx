import {FC} from "react";
import {IIcon} from "components/ui/icon/index.interfaces";

export const Icon: FC<IIcon> = ({ name, width=24, height=24 }) => {
    return (
        <svg
            version='1.1'
            xmlns='http://www.w3.org/2000/svg'
            style={{
                width: `${width}px`,
                height: `${height}px`,
            }}
        >
            <use xlinkHref={`/sprite.svg#${name}`}></use>
        </svg>
    );
}

export interface IIcon {
    name: string,
    width?: string | number,
    height?: string | number,
}

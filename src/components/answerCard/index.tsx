import { FC, useEffect, useState } from "react";
import styles from 'components/answerCard/index.module.sass';
import { IAnswerCard } from 'components/answerCard/index.interfaces';

export const AnswerCard: FC<IAnswerCard> = (
    {answer: {text, emphasis_start, emphasis_end}}
) => {
    const [textBefore, setTextBefore] = useState<string>('');
    const [textEmphasis, setTextEmphasis] = useState<string>('');
    const [textAfter, setTextAfter] = useState<string>('');

    useEffect(() => {
        setTextBefore(text.slice(0, emphasis_start));
        setTextEmphasis(text.slice(emphasis_start, emphasis_end));
        setTextAfter(text.slice(emphasis_end));
        console.log('before', textBefore);
        console.log('emphasis', textEmphasis);
        console.log('after', textAfter);
    })

    return (
        <div className={styles.answerContainer}>
            {
                textEmphasis.startsWith('Http://commons.wikimedia.org') ? 
                <>{
                    textEmphasis.split(', ').map((imageSrc) => {
                        return (
                            <ul>
                                <li>
                                    <img src={imageSrc} /> 
                                </li>
                            </ul>
                        )
                    })
                }
                    <p> {textAfter} </p> 
                </>
                : <p> {textBefore} <em>{textEmphasis}</em> {textAfter}</p>
            }
        </div>
    )
}

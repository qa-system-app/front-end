export interface IAnswer {
    text: string,
    emphasis_start: number,
    emphasis_end: number,
}

export interface IAnswerCard {
    answer: IAnswer,
}

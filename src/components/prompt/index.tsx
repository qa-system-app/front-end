import {IPrompt} from "components/prompt/index.interfaces";
import styles from 'components/prompt/index.module.sass';
import { Icon } from "components/ui/icon";
import {FC} from "react";

export const Prompt: FC<IPrompt> = ({
    text,
    onClick,
}) => {
    return (
        <button
            onClick={onClick}
            type='button'
            className={styles.button}
        >
            <Icon name="search" width='16' height='16'/>
            <p>{text}</p>
        </button>
    )
}

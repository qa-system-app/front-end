export interface IPrompt {
    text: string,
    onClick?: () => void,
}
